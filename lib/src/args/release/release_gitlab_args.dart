// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:args/args.dart';
import 'package:logging/logging.dart';
import 'package:pub_semver/pub_semver.dart';
import 'package:quiver/iterables.dart';

class ReleaseGitLabArgs {
  static const assetNameIndex = 0;
  static const assetUrlIndex = 1;
  static const assetTypeIndex = 2;

  static const _versionArg = 'version';
  static const _tagFormatArg = 'tag-format';
  static const _languageArg = 'language';
  static const _changelogPathArg = 'changelog-path';
  static const _projectIdArg = 'project-id';
  static const _gitLabUrlArg = 'gitlab-url';
  static const _gitLabApiKeyArg = 'gitlab-api-key';
  static const _mediaUrlFormatArg = 'media-url-format';
  static const _assetNameArg = 'asset-name';
  static const _assetUrlArg = 'asset-url';
  static const _assetTypeArg = 'asset-type';

  ReleaseGitLabArgs(ArgResults argResults)
      : version = Version.parse(argResults[_versionArg]),
        tagFormat = argResults[_tagFormatArg],
        language = argResults[_languageArg],
        changelogPath = argResults[_changelogPathArg],
        projectId = argResults[_projectIdArg],
        gitLabUrl = argResults[_gitLabUrlArg],
        gitLabApiKey = argResults[_gitLabApiKeyArg],
        mediaUrlFormat = argResults[_mediaUrlFormatArg],
        assets = _parseAssets(argResults);

  static final _logger = Logger('GitLabReleaseArgs');

  final Version version;
  final String tagFormat;
  final String language;
  final String changelogPath;
  final String projectId;
  final String gitLabUrl;
  final String gitLabApiKey;
  final String mediaUrlFormat;
  final List<List<String>> assets;

  static void setArgs(ArgParser argParser) {
    argParser
      ..addOption(
        _versionArg,
        help: 'Semantic version number to release.',
      )
      ..addOption(
        _tagFormatArg,
        defaultsTo: 'v{version}',
        help: 'Format to use for tags.\n'
            'Available placeholders: {version}.',
      )
      ..addOption(
        _languageArg,
        help: 'Language to use for the changelog.\n'
            'With fallback to the default language.',
      )
      ..addOption(
        _changelogPathArg,
        defaultsTo: 'changelog.yaml',
        help: 'Path to the changelog file.',
      )
      ..addOption(
        _projectIdArg,
        help: 'GitLab project ID to create the release for.',
      )
      ..addOption(
        _gitLabUrlArg,
        help: 'GitLab instance URL.',
      )
      ..addOption(
        _gitLabApiKeyArg,
        help: 'GitLab API key.',
      )
      ..addOption(
        _mediaUrlFormatArg,
        defaultsTo: '{filename}',
        help: 'Format to use for media URLs.\n'
            'Available placeholders: {filename}, {version} and {language}.',
      )
      ..addMultiOption(
        _assetNameArg,
        help: 'Name of the asset that should be added to the release.\n'
            'Must have corresponding --asset-url and --asset-type arguments.\n'
            'May be specified multiple times.',
      )
      ..addMultiOption(
        _assetUrlArg,
        help: 'URL of the asset that should be added to the release.\n'
            'Must have corresponding --asset-name and --asset-type arguments.\n'
            'May be specified multiple times.',
      )
      ..addMultiOption(
        _assetTypeArg,
        help: 'Type of the asset that should be added to the release.\n'
            'Must have corresponding --asset-name and --asset-url arguments.\n'
            'May be specified multiple times.',
      );
  }

  static List<List<String>> _parseAssets(ArgResults argResults) {
    final names = argResults[_assetNameArg] as List<String>;
    final urls = argResults[_assetUrlArg] as List<String>;
    final types = argResults[_assetTypeArg] as List<String>;

    if ({names.length, urls.length, types.length}.length != 1) {
      _logger.warning('Asset arguments mismatch, ignoring assets');
      return [];
    }

    return zip([names, urls, types]);
  }
}

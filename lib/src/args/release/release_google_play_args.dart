// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:args/args.dart';
import 'package:pub_semver/pub_semver.dart';

class ReleaseGooglePlayArgs {
  static const _versionArg = 'version';
  static const _changelogPathArg = 'changelog-path';
  static const _serviceAccountPathArg = 'service-account-path';
  static const _appBundlePathArg = 'app-bundle-path';
  static const _packageNameArg = 'package-name';
  static const _statusArg = 'status';
  static const _trackArg = 'track';

  ReleaseGooglePlayArgs(ArgResults argResults)
      : version = Version.parse(argResults[_versionArg]),
        changelogPath = argResults[_changelogPathArg],
        serviceAccountPath = argResults[_serviceAccountPathArg],
        appBundlePath = argResults[_appBundlePathArg],
        packageName = argResults[_packageNameArg],
        status = argResults[_statusArg],
        track = argResults[_trackArg];

  final Version version;
  final String changelogPath;
  final String serviceAccountPath;
  final String appBundlePath;
  final String packageName;
  final String status;
  final String track;

  static void setArgs(ArgParser argParser) {
    argParser
      ..addOption(
        _versionArg,
        help: 'Semantic version number to release.',
      )
      ..addOption(
        _changelogPathArg,
        defaultsTo: 'changelog.yaml',
        help: 'Path to the changelog file.',
      )
      ..addOption(
        _serviceAccountPathArg,
        defaultsTo: 'service-account.json',
        help: 'Path to the Google service account credentials.',
      )
      ..addOption(
        _appBundlePathArg,
        defaultsTo: 'app.aab',
        help: 'Path to the Android App Bundle to release.',
      )
      ..addOption(
        _packageNameArg,
        help: 'Package name (app ID) to create a release for.',
      )
      ..addOption(
        _statusArg,
        allowed: ['completed', 'draft', 'halted', 'inProgress'],
        help: 'Status to use for the release.',
      )
      ..addOption(
        _trackArg,
        allowed: ['internal', 'alpha', 'beta', 'production'],
        help: 'Track to create a release for.',
      );
  }
}

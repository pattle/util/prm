// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:args/args.dart';

class ChangelogGenerateArgs {
  static const _languageArg = 'language';
  static const _changelogPathArg = 'changelog-path';
  static const _changelogOutputPathArg = 'changelog-output-path';
  static const _mediaUrlFormatArg = 'media-url-format';

  ChangelogGenerateArgs(ArgResults argResults)
      : language = argResults[_languageArg],
        changelogPath = argResults[_changelogPathArg],
        changelogOutputPath = argResults[_changelogOutputPathArg],
        mediaUrlFormat = argResults[_mediaUrlFormatArg];

  final String language;
  final String changelogPath;
  final String changelogOutputPath;
  final String mediaUrlFormat;

  static void setArgs(ArgParser argParser) {
    argParser
      ..addOption(
        _languageArg,
        help: 'Language to use for the changelog.\n'
            'With fallback to the default language.',
      )
      ..addOption(
        _changelogPathArg,
        defaultsTo: 'changelog.yaml',
        help: 'Path to the changelog file.',
      )
      ..addOption(
        _changelogOutputPathArg,
        defaultsTo: 'CHANGELOG.md',
        help: 'Path to the generated changelog file.',
      )
      ..addOption(
        _mediaUrlFormatArg,
        defaultsTo: '{filename}',
        help: 'Format to use for media URLs.\n'
            'Available placeholders: {filename}, {version} and {language}.',
      );
  }
}

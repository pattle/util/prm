// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:io';

import 'package:pub_semver/pub_semver.dart';
import 'package:checked_yaml/checked_yaml.dart';

import '../models/changelog_entry.dart';

class ChangelogHelper {
  ChangelogHelper._(this.changelog, this.versions);

  final Map<Version, ChangelogEntry> changelog;
  final List<Version> versions;

  static Future<ChangelogHelper> create(String changelogPath) async {
    final changelogYaml = await File(changelogPath).readAsString();
    final changelog = _parseChangelog(changelogYaml);
    final versions = _sortVersions(changelog);

    return ChangelogHelper._(changelog, versions);
  }

  static Map<Version, ChangelogEntry> _parseChangelog(String changelogYaml) {
    return checkedYamlDecode(
      changelogYaml,
      (yaml) => yaml.map((key, value) {
        final version = Version.parse(key);
        final entry = ChangelogEntry.fromJson(Map.from(value));

        return MapEntry(version, entry);
      }),
    );
  }

  static List<Version> _sortVersions(Map<Version, ChangelogEntry> changelog) {
    return changelog.keys.toList()..sort();
  }
}

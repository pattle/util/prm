// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:meta/meta.dart';

class ChangelogEntryChange {
  ChangelogEntryChange({
    @required this.title,
    this.note,
    this.issue,
    this.credit,
  });

  final String title;
  final String note;
  final int issue;
  final String credit;

  static List<ChangelogEntryChange> convert(Iterable<dynamic> json) {
    return json.map((item) {
      if (item is String) {
        return ChangelogEntryChange(title: item);
      }

      final title = item.keys.first;

      return ChangelogEntryChange(
        title: title,
        note: item['note'],
        issue: item['issue'],
        credit: item['credit'],
      );
    }).toList();
  }
}

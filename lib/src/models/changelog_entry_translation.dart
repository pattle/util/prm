// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:json_annotation/json_annotation.dart';

import '../models/changelog_entry_change.dart';

part 'changelog_entry_translation.g.dart';

@JsonSerializable(anyMap: true, createToJson: false)
class ChangelogEntryTranslation {
  ChangelogEntryTranslation({this.note, this.changes, this.media});

  final String note;
  @JsonKey(fromJson: ChangelogEntryChange.convert)
  final List<ChangelogEntryChange> changes;
  final List<String> media;

  factory ChangelogEntryTranslation.fromJson(Map<String, dynamic> json) =>
      _$ChangelogEntryTranslationFromJson(json);
}

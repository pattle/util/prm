// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:json_annotation/json_annotation.dart';

import '../../models/google/tracks_resource_country_targeting.dart';
import '../../models/google/tracks_resource_release_note.dart';

part 'tracks_resource_release.g.dart';

@JsonSerializable()
class TracksResourceRelease {
  TracksResourceRelease({
    this.name,
    this.versionCodes,
    this.userFraction,
    this.countryTargeting,
    this.releaseNotes,
    this.status,
  });

  final String name;
  final List<String> versionCodes;
  final double userFraction;
  final TracksResourceCountryTargeting countryTargeting;
  final List<TracksResourceReleaseNote> releaseNotes;
  final String status;

  factory TracksResourceRelease.fromJson(Map<String, dynamic> json) =>
      _$TracksResourceReleaseFromJson(json);

  Map<String, dynamic> toJson() => _$TracksResourceReleaseToJson(this);
}

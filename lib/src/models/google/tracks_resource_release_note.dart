// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:json_annotation/json_annotation.dart';

part 'tracks_resource_release_note.g.dart';

@JsonSerializable()
class TracksResourceReleaseNote {
  TracksResourceReleaseNote({this.language, this.text});

  final String language;
  final String text;

  factory TracksResourceReleaseNote.fromJson(Map<String, dynamic> json) =>
      _$TracksResourceReleaseNoteFromJson(json);

  Map<String, dynamic> toJson() => _$TracksResourceReleaseNoteToJson(this);
}

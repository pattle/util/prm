// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:json_annotation/json_annotation.dart';

part 'edits_resource.g.dart';

@JsonSerializable()
class EditsResource {
  EditsResource({this.id, this.expiryTimeSeconds});

  final String id;
  final String expiryTimeSeconds;

  factory EditsResource.fromJson(Map<String, dynamic> json) =>
      _$EditsResourceFromJson(json);

  Map<String, dynamic> toJson() => _$EditsResourceToJson(this);
}

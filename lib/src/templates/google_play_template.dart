// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:pub_semver/pub_semver.dart';

import '../models/changelog_entry.dart';
import '../templates/template.dart';

class GooglePlayTemplate extends Template {
  static const _maxCharacters = 500;

  GooglePlayTemplate(Map<Version, ChangelogEntry> changelog, {String language})
      : super(changelog, language: language);

  @override
  String parse(Version version) {
    final output = StringBuffer();

    _writeHeader(output, version);
    output.writeln();
    _writeChanges(output, version);

    return output.toString();
  }

  void _writeHeader(StringBuffer output, Version version) {
    final note = noteFor(version);

    if (note != null) {
      output.writeln(note);
    }
  }

  void _writeChanges(StringBuffer output, Version version) {
    final changes = changesFor(version);

    for (final change in changes) {
      final text = '• ${change.title}\n';

      if (output.length + text.length >= _maxCharacters) {
        break;
      }

      output.write(text);
    }
  }
}

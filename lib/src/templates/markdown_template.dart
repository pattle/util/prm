// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:convert';

import 'package:pub_semver/pub_semver.dart';

import '../helpers/list_helper.dart';
import '../models/changelog_entry.dart';
import '../models/changelog_entry_change.dart';
import '../templates/template.dart';

class MarkdownTemplate extends Template {
  MarkdownTemplate(
    Map<Version, ChangelogEntry> changelog, {
    String mediaUrlFormat,
    String language,
  }) : super(changelog, mediaUrlFormat: mediaUrlFormat, language: language);

  @override
  String parse(Version version) {
    final output = StringBuffer();

    final note = noteFor(version);
    final changes = changesFor(version);
    final media = mediaFor(version);

    output.writeln('# $version');
    _writeHeader(output, note);
    output.writeln();
    _writeChanges(output, changes, note != null);
    output.writeln();
    _writeMedia(output, media, version);

    return output.toString();
  }

  void _writeHeader(StringBuffer output, String note) {
    if (note == null) return;

    output.writeln();
    output.writeln(note);
  }

  void _writeChanges(
    StringBuffer output,
    List<ChangelogEntryChange> changes,
    bool showHeader,
  ) {
    if (changes.isNullOrEmpty) return;
    if (showHeader) output.writeln('## Changes');

    for (final change in changes) {
      output.write('* ${change.title}');

      if (change.issue != null) output.write(' (#${change.issue})');
      if (change.credit != null) output.write(' (thanks to ${change.credit})');
      if (change.note != null) _writeChangeNote(output, change.note);

      output.writeln();
    }
  }

  void _writeChangeNote(StringBuffer output, String note) {
    for (final line in LineSplitter.split(note)) {
      output.write('  \n  $line');
    }
  }

  void _writeMedia(StringBuffer output, List<String> media, Version version) {
    if (media.isNullOrEmpty) return;

    for (final mediaItem in media) {
      final mediaUrl = generateMediaUrl(version, mediaItem);
      output.writeln('![Media]($mediaUrl)');
    }
  }
}

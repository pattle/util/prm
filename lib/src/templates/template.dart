// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:meta/meta.dart';
import 'package:pub_semver/pub_semver.dart';

import '../helpers/map_helper.dart';
import '../helpers/string_helper.dart';
import '../models/changelog_entry.dart';
import '../models/changelog_entry_change.dart';

abstract class Template {
  Template(this._changelog, {this.mediaUrlFormat, this.language});

  final Map<Version, ChangelogEntry> _changelog;

  @protected
  final String mediaUrlFormat;
  @protected
  final String language;

  String parse(Version version);

  @protected
  String noteFor(Version version) =>
      _changelog[version].translations?.get(language)?.note ??
      _changelog[version].note;

  @protected
  List<ChangelogEntryChange> changesFor(Version version) =>
      _changelog[version].translations?.get(language)?.changes ??
      _changelog[version].changes;

  @protected
  List<String> mediaFor(Version version) =>
      _changelog[version].translations?.get(language)?.media ??
      _changelog[version].media;

  @protected
  bool hasMediaFor(Version version) =>
      _changelog[version].translations?.get(language)?.media != null;

  @protected
  String generateMediaUrl(Version version, String filename) {
    if (mediaUrlFormat == null) return '';

    return mediaUrlFormat.format({
      'filename': filename,
      'version': version.toString(),
      'language': hasMediaFor(version) ? language : null,
    });
  }
}

// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import '../../runners/release/release_gitlab_runner.dart';
import '../../runners/release/release_google_play_runner.dart';
import '../../runners/runner.dart';

class ReleaseRunner extends Runner {
  ReleaseRunner() {
    addSubcommand(ReleaseGitLabRunner());
    addSubcommand(ReleaseGooglePlayRunner());
  }

  @override
  final String name = 'release';

  @override
  final String description = 'Manage releases.';
}

// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:io';

import 'package:chopper/chopper.dart';
import 'package:googleapis_auth/auth_io.dart';
import 'package:logging/logging.dart';

import '../../args/release/release_google_play_args.dart';
import '../../converters/typed_json_converter.dart';
import '../../helpers/changelog_helper.dart';
import '../../interceptors/force_success_interceptor.dart';
import '../../models/google/bundles_resource.dart';
import '../../models/google/edits_resource.dart';
import '../../models/google/tracks_resource.dart';
import '../../models/google/tracks_resource_release.dart';
import '../../models/google/tracks_resource_release_note.dart';
import '../../runners/runner.dart';
import '../../services/bundles_service.dart';
import '../../services/edits_service.dart';
import '../../services/tracks_service.dart';
import '../../templates/google_play_template.dart';

class ReleaseGooglePlayRunner extends Runner {
  ReleaseGooglePlayRunner() {
    ReleaseGooglePlayArgs.setArgs(argParser);
  }

  static final _logger = Logger('ReleaseGooglePlayRunner');

  @override
  final String name = 'google-play';

  @override
  final String description = 'Release on Google Play.';

  @override
  Future run() async {
    await super.run();

    final args = ReleaseGooglePlayArgs(argResults);
    final apiClient = await _createApiClient(args);

    final edits = await _insertEdit(args, apiClient);
    final bundles = await _uploadBundle(args, apiClient, edits.id);

    await _updateTrack(args, apiClient, edits.id, bundles.versionCode);
    await _commitEdit(args, apiClient, edits.id);
  }

  Future<EditsResource> _insertEdit(
    ReleaseGooglePlayArgs args,
    ChopperClient apiClient,
  ) async {
    final editsService = EditsService.create(apiClient);

    _logger.info('Inserting edit...');
    final edits = await editsService.insert(
      packageName: args.packageName,
    );

    return edits.body;
  }

  Future<BundlesResource> _uploadBundle(
    ReleaseGooglePlayArgs args,
    ChopperClient apiClient,
    String editId,
  ) async {
    final bundlesService = BundlesService.create(apiClient);
    final aab = File(args.appBundlePath).openRead();

    _logger.info('Uploading bundle...');
    final bundles = await bundlesService.upload(
      packageName: args.packageName,
      editId: editId,
      uploadType: 'media',
      aab: aab,
    );

    return bundles.body;
  }

  Future<TracksResourceRelease> _createRelease(
    ReleaseGooglePlayArgs args,
    int versionCode,
  ) async {
    final changelogHelper = await ChangelogHelper.create(args.changelogPath);
    final googlePlayTemplate = GooglePlayTemplate(changelogHelper.changelog);

    return TracksResourceRelease(
      name: args.version.toString(),
      versionCodes: [
        versionCode.toString(),
      ],
      releaseNotes: [
        TracksResourceReleaseNote(
          language: 'en-US',
          text: googlePlayTemplate.parse(args.version),
        )
      ],
      status: args.status,
    );
  }

  Future<TracksResource> _updateTrack(
    ReleaseGooglePlayArgs args,
    ChopperClient apiClient,
    String editId,
    int versionCode,
  ) async {
    final tracksService = TracksService.create(apiClient);
    final release = await _createRelease(args, versionCode);

    _logger.info('Updating track...');
    final tracks = await tracksService.update(
      packageName: args.packageName,
      editId: editId,
      track: args.track,
      tracksResource: TracksResource(
        track: args.track,
        releases: [
          release,
        ],
      ),
    );

    return tracks.body;
  }

  Future<EditsResource> _commitEdit(
    ReleaseGooglePlayArgs args,
    ChopperClient apiClient,
    String editId,
  ) async {
    final editsService = EditsService.create(apiClient);

    _logger.info('Comitting edit...');
    final edits = await editsService.commit(
      packageName: args.packageName,
      editId: editId,
    );

    return edits.body;
  }

  Future<ChopperClient> _createApiClient(ReleaseGooglePlayArgs args) async {
    final json = await File(args.serviceAccountPath).readAsString();
    final serviceAccount = ServiceAccountCredentials.fromJson(json);
    final scopes = ['https://www.googleapis.com/auth/androidpublisher'];
    final client = await clientViaServiceAccount(serviceAccount, scopes);

    return ChopperClient(
      baseUrl: 'https://www.googleapis.com',
      client: client,
      interceptors: [ForceSuccessInterceptor()],
      converter: TypedJsonConverter({
        EditsResource: (jsonData) => EditsResource.fromJson(jsonData),
        BundlesResource: (jsonData) => BundlesResource.fromJson(jsonData),
        TracksResource: (jsonData) => TracksResource.fromJson(jsonData),
      }),
    );
  }
}

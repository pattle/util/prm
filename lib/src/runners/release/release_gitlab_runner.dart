// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'package:chopper/chopper.dart';
import 'package:logging/logging.dart';

import '../../args/release/release_gitlab_args.dart';
import '../../helpers/changelog_helper.dart';
import '../../helpers/string_helper.dart';
import '../../interceptors/force_success_interceptor.dart';
import '../../interceptors/private_token_interceptor.dart';
import '../../models/gitlab/release.dart';
import '../../models/gitlab/release_assets.dart';
import '../../models/gitlab/release_link.dart';
import '../../runners/runner.dart';
import '../../services/releases_service.dart';
import '../../templates/markdown_template.dart';

class ReleaseGitLabRunner extends Runner {
  ReleaseGitLabRunner() {
    ReleaseGitLabArgs.setArgs(argParser);
  }

  static final _logger = Logger('ReleaseGitLabRunner');

  @override
  final String name = 'gitlab';

  @override
  final String description = 'Release on GitLab.';

  @override
  Future run() async {
    await super.run();

    final args = ReleaseGitLabArgs(argResults);
    final apiClient = await _createApiClient(args);
    final releasesService = ReleasesService.create(apiClient);

    final release = await _createRelease(args);

    _logger.info('Creating release for ${args.version}...');
    await releasesService.insert(
      id: args.projectId,
      release: release,
    );
  }

  Future<Release> _createRelease(ReleaseGitLabArgs args) async {
    final changelogHelper = await ChangelogHelper.create(args.changelogPath);

    final markdownTemplate = MarkdownTemplate(
      changelogHelper.changelog,
      mediaUrlFormat: args.mediaUrlFormat,
      language: args.language,
    );

    return Release(
      name: args.version.toString(),
      tagName: args.tagFormat.format({
        'version': args.version.toString(),
      }),
      description: markdownTemplate.parse(args.version),
      assets: ReleaseAssets(
        links: _createLinks(args),
      ),
    );
  }

  List<ReleaseLink> _createLinks(ReleaseGitLabArgs args) {
    return args.assets.map(
      (asset) => ReleaseLink(
        name: asset[ReleaseGitLabArgs.assetNameIndex],
        url: asset[ReleaseGitLabArgs.assetUrlIndex],
        linkType: asset[ReleaseGitLabArgs.assetTypeIndex],
      ),
    );
  }

  Future<ChopperClient> _createApiClient(ReleaseGitLabArgs args) async {
    return ChopperClient(
      baseUrl: args.gitLabUrl,
      interceptors: [
        PrivateTokenInterceptor(args.gitLabApiKey),
        ForceSuccessInterceptor(),
      ],
      converter: JsonConverter(),
    );
  }
}

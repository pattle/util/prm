// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:io';

import 'package:logging/logging.dart';

import '../../args/changelog/changelog_generate_args.dart';
import '../../helpers/changelog_helper.dart';
import '../../runners/runner.dart';
import '../../templates/markdown_template.dart';

class ChangelogGenerateRunner extends Runner {
  ChangelogGenerateRunner() {
    ChangelogGenerateArgs.setArgs(argParser);
  }

  static final _logger = Logger('ChangelogGenerateRunner');

  @override
  final String name = 'generate';

  @override
  final String description = 'Generate a changelog.';

  @override
  Future run() async {
    await super.run();

    final content = StringBuffer();
    final args = ChangelogGenerateArgs(argResults);
    final changelogHelper = await ChangelogHelper.create(args.changelogPath);

    final versions = changelogHelper.versions;
    final markdownTemplate = MarkdownTemplate(
      changelogHelper.changelog,
      mediaUrlFormat: args.mediaUrlFormat,
      language: args.language,
    );

    for (final version in versions.reversed) {
      content.writeln(markdownTemplate.parse(version));
    }

    _logger.info('Writing changelog for ${versions.length} versions...');
    await _writeChangelog(args, content);
  }

  Future _writeChangelog(
    ChangelogGenerateArgs args,
    StringBuffer content,
  ) async {
    final changelogText = content.toString();
    final file = File(args.changelogOutputPath);
    await file.create(recursive: true);
    await file.writeAsString(changelogText.trim());
  }
}

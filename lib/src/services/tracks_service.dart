// Copyright (C) 2020  Louis Matthijssen
//
// This file is part of Pattle Release Manager.
//
// Pattle Release Manager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pattle Release Manager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pattle Release Manager.  If not, see <https://www.gnu.org/licenses/>.

import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:meta/meta.dart';

import '../models/google/tracks_resource.dart';

part 'tracks_service.chopper.dart';

@ChopperApi(baseUrl: '/androidpublisher/v3/applications')
abstract class TracksService extends ChopperService {
  @Get(path: '/{packageName}/edits/{editId}/tracks/{track}')
  Future<Response<TracksResource>> get({
    @required @Path() String packageName,
    @required @Path() String editId,
    @required @Path() String track,
  });

  @Put(path: '/{packageName}/edits/{editId}/tracks/{track}')
  Future<Response<TracksResource>> update({
    @required @Path() String packageName,
    @required @Path() String editId,
    @required @Path() String track,
    @required @Body() TracksResource tracksResource,
  });

  static TracksService create([ChopperClient client]) =>
      _$TracksService(client);
}

FROM google/dart:dev AS build

WORKDIR /build
COPY . .

RUN pub get && \
    pub run build_runner build && \
    dart2native lib/src/main.dart -o prm

FROM gcr.io/distroless/base-debian10

WORKDIR /app
COPY --from=build /build/prm .

CMD ["/app/prm"]
